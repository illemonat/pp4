


while true
    args = ["currentNumber", "numberDifference"];
    numberData = struct();
    for arg = args
        prompt = sprintf("%s: ", arg);
        numberData.(arg) = input(prompt);
    end
    sendNumberData(numberData);
end


function resp = sendNumberData(numberData) 
    import matlab.net.http.*
    import matlab.net.http.field.*
    import matlab.net.*

    method = RequestMethod.POST;

    contentTypeField = matlab.net.http.field.ContentTypeField('application/json');

    header = [contentTypeField];

    body = matlab.net.http.MessageBody(numberData);

    request = matlab.net.http.RequestMessage(method,header,body);

    uri = URI('http://localhost:8080/add');

    resp = send(request,uri);
end
