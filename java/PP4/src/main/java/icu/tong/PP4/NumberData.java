package icu.tong.PP4;


public class NumberData {
    private double currentNumber;
    private double numberDifference;

    NumberData(double currentNumber, double numberDifference) {
        this.currentNumber = currentNumber;
        this.numberDifference = numberDifference;
    }

    public double getCurrentNumber() {
        return this.currentNumber;
    }

    public double getNumberDifference() {
        return this.numberDifference;
    }

}
