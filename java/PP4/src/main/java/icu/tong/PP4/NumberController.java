package icu.tong.PP4;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NumberController {
    private final List<NumberData> data = new ArrayList<NumberData>();
    
    NumberController() {
        data.add(new NumberData(1, 1));
    }

	@GetMapping("/data")
    public List<NumberData> data() {
        return data;
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/add")
    public void addLocation(@RequestBody NumberData numberData) {
        data.add(numberData);
    }
}